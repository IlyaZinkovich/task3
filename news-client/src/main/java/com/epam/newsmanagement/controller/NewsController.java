package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.forms.Filter;
import com.epam.newsmanagement.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@SessionAttributes({"filter"})
public class NewsController {

    private static Logger logger = Logger.getLogger(NewsController.class);

    public final static Integer DEFAULT_PAGE_SIZE = 4;

    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private CommentService commentService;

    @ModelAttribute("filter")
    public Filter initFilterForm() {
        return new Filter();
    }

    @ModelAttribute("commentForm")
    public Comment initCommentForm() {
        return new Comment();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model, @ModelAttribute Filter filter) {
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/news-list", method = RequestMethod.GET)
    public String newsList(Model model, @ModelAttribute Filter filter) {
        return newsListPaged(model, 1, filter);
    }

    @RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
    public String newsListPaged(Model model, @PathVariable Integer page, @ModelAttribute Filter filter) {
        SearchCriteria searchCriteria = filterToSearchCriteria(page, filter);
        List<News> newsList = newsService.findNews(searchCriteria);
        List<Author> authorList = authorService.findHavingNews();
        List<Tag> tagList = tagService.findAll();
        int newsCount = newsService.getNewsCount(searchCriteria);
        model.addAttribute("newsCount", newsCount);
        Integer pageCount = (int) Math.ceil((double) newsCount / DEFAULT_PAGE_SIZE);
        fillModelAttributesForNewsList(model, filter, newsList, authorList, tagList, pageCount);
        return "news-list";
    }

    private void fillModelAttributesForNewsList(Model model, @ModelAttribute Filter filter, List<News> newsList, List<Author> authorList, List<Tag> tagList, Integer pageCount) {
        model.addAttribute("newsList", newsList);
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("pageCount", pageCount);
        model.addAttribute("filter", filter);
        model.addAttribute("current", "news-list");
    }

    private SearchCriteria filterToSearchCriteria(Integer page, Filter filter) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(filter.getAuthorId());
        searchCriteria.setTagsId(filter.getTagsId());
        if (page != null) {
            searchCriteria.setBegin((page - 1) * DEFAULT_PAGE_SIZE);
            searchCriteria.setEnd(page * DEFAULT_PAGE_SIZE);
        }
        return searchCriteria;
    }

    @RequestMapping(value = "/view-news/{id}", method = RequestMethod.GET)
    public String viewNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        News news = newsService.findById(id);
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        addPreviousAndNextNewsIdToTheModel(model, previousAndNextNews);
        model.addAttribute("news", news);
        return "view-news";
    }

    private void addPreviousAndNextNewsIdToTheModel(Model model, List<Long> previousAndNextNews) {
        if (previousAndNextNews.isEmpty()) return;
        if (previousAndNextNews.get(0) != null) {
            model.addAttribute("previous", previousAndNextNews.get(0));
        }
        if (previousAndNextNews.get(1) != null) {
            model.addAttribute("next", previousAndNextNews.get(1));
        }
    }

    @RequestMapping(value = "/view-news/{newsId}/add-comment", method = RequestMethod.POST)
    public String addComment(@PathVariable Long newsId, Model model, @ModelAttribute Comment comment) {
        if (comment.getCommentText() == null) {
            return "redirect:/view-news/" + newsId;
        }
        comment.setCreationDate(new Date());
        News news = newsService.findById(newsId);
        comment.setNews(news);
        commentService.addComment(comment);
        news.getComments().add(comment);
        newsService.saveNews(news);
        model.addAttribute("news", news);
        return "redirect:/view-news/" + newsId;
    }

    @RequestMapping(value = "/view-news/{id}/previous")
    public String previousNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        Long previousNewsId = previousAndNextNews.get(0);
        News news = newsService.findById(previousNewsId);
        model.addAttribute("news", news);
        return "redirect:/view-news/" + previousNewsId;
    }

    @RequestMapping(value = "/view-news/{id}/next")
    public String nextNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        Long previousNewsId = previousAndNextNews.get(1);
        News news = newsService.findById(previousNewsId);
        model.addAttribute("news", news);
        return "redirect:/view-news/" + previousNewsId;
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.POST)
    public String filterNews(Model model, @ModelAttribute Filter filter, BindingResult result) {
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.GET)
    public String showFilteredNews() {
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/news-list/filter/reset", method = RequestMethod.GET)
    public String showNews(Model model) {
        model.addAttribute("filter", new Filter());
        return "redirect:/news-list";
    }
}
