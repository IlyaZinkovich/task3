<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../pages/taglib.jsp"%>
<html>
    <head>
        <link rel="stylesheet" href="<c:url value='/resources/css/datepicker.css'/>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>

        <title><tiles:getAsString name="title"/></title>
    </head>

    <body>
        <div class="container">
            <tiles:insertAttribute name="header"/>
            <div class="row">
                <div class="col-md-12 main">
                    <tiles:insertAttribute name="body"/>
                </div>
            </div>
        </div>
        <tiles:insertAttribute name="footer"/>
    </body>
</html>