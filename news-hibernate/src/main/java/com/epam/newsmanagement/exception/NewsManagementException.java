package com.epam.newsmanagement.exception;

public class NewsManagementException extends Exception {
    private static final long serialVersionUID = -1424745208846935641L;

    public NewsManagementException() {
    }

    public NewsManagementException(Throwable cause) {
        super(cause);
    }
}
