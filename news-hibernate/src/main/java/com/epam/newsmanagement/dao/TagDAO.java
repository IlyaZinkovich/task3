package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.domain.Tag;

import java.util.List;

/**
 * A DatabaseAccessObject interface that
 * provides access to tags data in the data source.
 */
public interface TagDAO extends GenericDAO<Tag> {

    /**
     * Returns the list of tags
     * that news with the given id has.
     *
     * @param  newsId
     *         The news id
     *
     * @return  the list of tags
     * that news with the given id has
     *
     */
    List<Tag> findByNewsId(Long newsId);


    /**
     * Returns the list of tags
     * with ids from the given list.
     *
     * @param  tagsId
     *         The list of tags id
     *
     * @return  the list of tags
     * that news with the given id has
     *
     */
    List<Tag> findByTagsId(List<Long> tagsId);

}
