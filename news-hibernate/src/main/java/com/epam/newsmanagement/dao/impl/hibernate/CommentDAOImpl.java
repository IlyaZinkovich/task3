package com.epam.newsmanagement.dao.impl.hibernate;


import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.impl.hibernate.util.HibernateUtil;
import com.epam.newsmanagement.domain.Comment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentDAOImpl implements CommentDAO {

    private static final String FIND_COMMENTS_BY_NEWS_ID = "select distinct c from News n inner join n.comments c " +
            " where n.id=:newsId";
    private static final String NEWS_ID_PARAMETER = "newsId";

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long insert(Comment item) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(item);
        return item.getId();
    }

    @Override
    public void update(Comment item) {
        HibernateUtil.update(item, sessionFactory);
    }

    @Override
    public void delete(Long itemId) {
        HibernateUtil.deleteObjectOfClass(itemId, Comment.class, sessionFactory);
    }

    @Override
    public List<Comment> findAll() {
        return HibernateUtil.findAllOfClass(Comment.class, sessionFactory);
    }

    @Override
    public Comment findById(Long commentId) {
        return (Comment) HibernateUtil.findByIdOfClass(commentId, Comment.class, sessionFactory);
    }

    @Override
    public List<Comment> findByNewsId(Long newsId) {
        return HibernateUtil.findListFromQuery((Session session) ->
                session.createQuery(FIND_COMMENTS_BY_NEWS_ID)
                .setParameter(NEWS_ID_PARAMETER, newsId), sessionFactory);
    }

}
