package com.epam.newsmanagement.dao.exception;

public class DAOException extends Exception {
    private static final long serialVersionUID = 398015075332272981L;

    public DAOException() {
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
