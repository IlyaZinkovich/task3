package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;

import java.util.List;

/**
 * A DatabaseAccessObject interface that
 * provides access to comments data in the data source.
 */
public interface CommentDAO extends GenericDAO<Comment> {

    /**
     * Returns the list of comments
     * that news with the given id has.
     *
     * @param  newsId
     *         The news id
     *
     * @return  the list of comments
     * that news with the given id has
     *
     */
    List<Comment> findByNewsId(Long newsId);
}
