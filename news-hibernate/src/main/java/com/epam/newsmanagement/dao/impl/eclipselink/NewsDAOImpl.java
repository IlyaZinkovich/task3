package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class NewsDAOImpl implements NewsDAO {

    private static final String FIND_NEWS_BEGIN = "select distinct n from News n ";
    private static final String FIND_NEWS_ID_LIST_BEGIN = "select distinct n.id from News n ";
    private static final String FIND_NEWS_COUNT_BEGIN = "select count(distinct n.id) from News n ";
    private static final String FIND_NEWS_BY_AUTHOR = "inner join n.author a ";
    private static final String WHERE_AUTHOR = " a.id=:authorId ";
    private static final String WHERE_TAGS = " t.id in :tagsId ";
    private static final String FIND_NEWS_BY_TAGS = "inner join n.tags t ";
    private static final String ORDER_BY_NUMBER_OF_COMMENTS = "order by size(n.comments) desc, n.title asc";
    private static final String WHERE = "where ";
    private static final String AND = "and ";
    private static final String AUTHOR_ID_PARAMETER = "authorId";
    private static final String TAGS_ID_PARAMETER = "tagsId";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria)  {
        searchCriteria.setBegin(null);
        searchCriteria.setEnd(null);
        List<Long> prevAndNext = new ArrayList<>();
        List<News> newsList = findNews(searchCriteria);
        List<Long> newsIdList = newsList.stream().map(News::getId).collect(Collectors.toList());
        for (int i = 0; i < newsIdList.size(); i++) {
            if (newsIdList.get(i).equals(newsId)) {
                if (i > 0) {
                    prevAndNext.add(newsIdList.get(i - 1));
                } else {
                    prevAndNext.add(0L);
                }
                if (i < newsIdList.size() - 1) {
                    prevAndNext.add(newsIdList.get(i + 1));
                } else {
                    prevAndNext.add(0L);
                }
                return prevAndNext;
            }
        }
        return Arrays.asList(0L, 0L);
    }

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) {
        Query query = entityManager.createQuery(buildFindQuery(searchCriteria, FIND_NEWS_BEGIN));
        setQueryParameters(searchCriteria, query);
        return query.getResultList();
    }

    private void setQueryParameters(SearchCriteria searchCriteria, Query query) {
        if (searchCriteria.getAuthorId() != null) {
            query.setParameter(AUTHOR_ID_PARAMETER, searchCriteria.getAuthorId());
        }
        if (searchCriteria.getTagsId() != null) {
            query.setParameter(TAGS_ID_PARAMETER, searchCriteria.getTagsId());
        }
        if (searchCriteria.getBegin() != null && searchCriteria.getEnd() != null) {
            query.setFirstResult(searchCriteria.getBegin());
            query.setMaxResults(searchCriteria.getEnd() - searchCriteria.getBegin());
        }
    }

    private String buildFindQuery(SearchCriteria searchCriteria, String findBegin) {
        StringBuilder queryBuilder = new StringBuilder();
        boolean firstWhere = true;
        queryBuilder.append(findBegin);
        if (searchCriteria.getAuthorId() != null) {
            queryBuilder.append(FIND_NEWS_BY_AUTHOR);
        }
        if (searchCriteria.getTagsId() != null) {
            queryBuilder.append(FIND_NEWS_BY_TAGS);
        }
        if (searchCriteria.getAuthorId() != null) {
            queryBuilder.append(firstWhere ? WHERE : AND);
            firstWhere = false;
            queryBuilder.append(WHERE_AUTHOR);
        }
        if (searchCriteria.getTagsId() != null) {
            queryBuilder.append(firstWhere ? WHERE : AND);
            queryBuilder.append(WHERE_TAGS);
        }
        queryBuilder.append(ORDER_BY_NUMBER_OF_COMMENTS);
        return queryBuilder.toString();
    }

    @Override
    public int getNewsCount() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        cq.select(cb.count(cq.from(News.class)));
        return entityManager.createQuery(cq).getSingleResult().intValue();
    }

    @Override
    public int getNewsCount(SearchCriteria searchCriteria) {
        searchCriteria.setBegin(null);
        searchCriteria.setEnd(null);
        Query query = entityManager.createQuery(buildFindQuery(searchCriteria, FIND_NEWS_COUNT_BEGIN), Integer.class);
        setQueryParameters(searchCriteria, query);
        List<Long> newsIdList = query.getResultList();
        return newsIdList.isEmpty() ? 0 : newsIdList.get(0).intValue();
    }

    @Override
    public Long insert(News item) {
        entityManager.persist(item);
        return item.getId();
    }

    @Override
    public void update(News item) {
        entityManager.merge(item);
    }

    @Override
    public void delete(Long itemId) {
        News news = entityManager.find(News.class, itemId);
        if (news != null) {
            entityManager.remove(news);
        }
    }

    @Override
    public List<News> findAll() {
        return findNews(new SearchCriteria());
    }

    @Override
    public News findById(Long id) {
        return entityManager.find(News.class, id);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
