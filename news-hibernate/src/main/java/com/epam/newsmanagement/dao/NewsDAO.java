package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;

import java.util.List;

/**
 * A DatabaseAccessObject interface that
 * provides access to news data in the data source.
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * Returns the news id list of news found by search criteria.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the List of news id
     *
     */
    List<News> findNews(SearchCriteria searchCriteria);

    /**
     * Returns the news id list of two news in the filtered by search criteria list
     * previous and next by one to the news with the given id.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the List of news id,
     *          the first element is 0 if the news with the given id is the first in the whole filtered news list
     *          the second element is 0 if the news with the given id is the last in the whole filtered news list
     *
     */
    List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria);

    /**
     * Returns the count of news found by search criteria.
     *
     *          the criteria that are used to find news
     *
     * @return  the integer count of news
     *
     */
    int getNewsCount();

    int getNewsCount(SearchCriteria searchCriteria);
}
