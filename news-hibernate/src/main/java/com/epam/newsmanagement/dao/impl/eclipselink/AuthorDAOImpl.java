package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Repository
public class AuthorDAOImpl implements AuthorDAO {

    private final static String FIND_ALL_AUTHORS_QUERY = "SELECT a FROM Author a";
    private final static String FIND_AUTHOR_HAVING_NEWS_QUERY = "SELECT DISTINCT a FROM News n INNER JOIN n.author a";
    private final static String FIND_AUTHOR_BY_NEWS_ID_QUERY = "SELECT a FROM News n INNER JOIN n.author a " +
            "WHERE n.id=:newsId";
    private final static String NEWS_ID_PARAMETER = "newsId";
    private final static String FIND_AUTHORS_NOT_EXPIRED_QUERY = "SELECT a FROM Author a WHERE a.expired IS NULL";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long insert(Author item) {
        entityManager.persist(item);
        return item.getId();
    }

    @Override
    public void update(Author item) {
        entityManager.merge(item);
    }

    @Override
    public void delete(Long itemId) {
        Author author = findById(itemId);
        if (author != null) {
            author.setExpired(new Date());
            entityManager.merge(author);
        }
    }

    @Override

    public List<Author> findAll() {
        List<Author> authorList = entityManager.createQuery(FIND_ALL_AUTHORS_QUERY).getResultList();
        return authorList;
    }

    @Override
    public Author findById(Long id) {
        Author author = entityManager.find(Author.class, id);
        return author;
    }

    @Override
    public Author findByNewsId(Long newsId) {
        List<Author> foundAuthors = entityManager.createQuery(FIND_AUTHOR_BY_NEWS_ID_QUERY)
                .setParameter(NEWS_ID_PARAMETER, newsId)
                .getResultList();
        if (foundAuthors == null) {
            return null;
        }
        return foundAuthors.isEmpty() ? null : foundAuthors.get(0);
    }

    @Override
    public List<Author> findNotExpired() {
        List<Author> foundAuthors =  entityManager.createQuery(FIND_AUTHORS_NOT_EXPIRED_QUERY, Author.class).getResultList();
        return foundAuthors;
    }

    @Override
    public List<Author> findHavingNews() {
        List<Author> foundAuthors = entityManager.createQuery(FIND_AUTHOR_HAVING_NEWS_QUERY).getResultList();
        return foundAuthors;
    }

    @Override
    public void update(Long authorId, Date expirationDate) {
        Author author = findById(authorId);
        author.setExpired(expirationDate);
        entityManager.merge(author);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}