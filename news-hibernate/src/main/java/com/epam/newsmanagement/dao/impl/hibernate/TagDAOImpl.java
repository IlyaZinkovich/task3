package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.impl.hibernate.util.HibernateUtil;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class TagDAOImpl implements TagDAO {

    private static final String FIND_TAGS_BY_NEWS_ID = "select t from News n inner join n.tags t " +
            " where n.id=:newsId";
    private static final String FIND_TAG_BY_NAME = "select t from Tag t where t.name is :name";
    private static final String FIND_TAGS_WITH_IDS = "select t from Tag t where t.id in (:tagsId)";
    private static final String NEWS_ID_PARAMETER = "newsId";
    private static final String NAME_PARAMETER = "name";
    private static final String TAGS_ID_PARAMETER = "tagsId";

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag findById(Long tagId) {
        return (Tag) HibernateUtil.findByIdOfClass(tagId, Tag.class, sessionFactory);
    }

    @Override
    public Long insert(Tag tag) {
        return HibernateUtil.insert(tag, sessionFactory);
    }

    @Override
    public void update(Tag tag) {
        HibernateUtil.update(tag, sessionFactory);
    }

    @Override
    public void delete(Long tagId) {
        Session session = sessionFactory.getCurrentSession();
        Tag tag = (Tag) session.get(Tag.class, tagId);
        if (tag != null) {
            Set<News> newsList = tag.getNews();
            newsList.forEach(news -> news.getTags().remove(tag));
            tag.setNews(null);
            session.delete(tag);
        }
    }

    @Override
    public List<Tag> findAll() {
        return HibernateUtil.findAllOfClass(Tag.class, sessionFactory);
    }

    @Override
    public List<Tag> findByNewsId(Long newsId) {
        return HibernateUtil.findListFromQuery((Session session) ->
                session.createQuery(FIND_TAGS_BY_NEWS_ID)
                .setParameter(NEWS_ID_PARAMETER, newsId), sessionFactory);
    }

    @Override
    public List<Tag> findByTagsId(List<Long> tagsId) {
        if (tagsId == null || tagsId.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        return HibernateUtil.findListFromQuery((Session session) ->
                        session.createQuery(FIND_TAGS_WITH_IDS).setParameterList(TAGS_ID_PARAMETER, tagsId),
                sessionFactory);
    }


}