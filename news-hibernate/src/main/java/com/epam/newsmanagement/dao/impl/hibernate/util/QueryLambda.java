package com.epam.newsmanagement.dao.impl.hibernate.util;

import org.hibernate.Query;
import org.hibernate.Session;

@FunctionalInterface
public interface QueryLambda {
    Query execute(Session session);
}