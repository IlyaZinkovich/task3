package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
public interface AuthorDAO extends GenericDAO<Author> {

    /**
     * Returns the {@code Author} object
     * representing the author that news with the given id has.
     *
     * @param  newsId
     *         The news id
     *
     * @return  the {@code Author} object representing the author
     * that news with the given id has
     *
     */
    Author findByNewsId(Long newsId);

    /**
     * Finds all the authors that are not expired
     *
     * @return  the list of authors that are not expired
     *
     */
    List<Author> findNotExpired();

    /**
     * Finds all the authors having news
     *
     * @return  the list of authors having news
     *
     */
    List<Author> findHavingNews();


    /**
     * Updates the author expiration date.
     *
     * @param  authorId
     *         The Author id
     *
     * @param  expirationDate
     *         The Author expiration date
     *
     */
    void update(Long authorId, Date expirationDate);
}
