package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TagDAOImpl implements TagDAO {

    private final static String FIND_ALL_TAGS_QUERY = "SELECT t FROM Tag t";
    private final static String FIND_TAG_BY_IDS_QUERY = "SELECT t FROM Tag t WHERE t.id in :tagsId";
    private final static String FIND_TAGS_BY_NEWS_ID_QUERY = "SELECT t FROM News n INNER JOIN n.tags t " +
            "WHERE n.id=:newsId";
    private final static String NEWS_ID_PARAMETER = "newsId";
    private final static String TAGS_ID_PARAMETER = "tagsId";

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Long insert(Tag item) {
        entityManager.persist(item);
        return item.getId();
    }

    @Override
    public void update(Tag item) {
        entityManager.merge(item);
    }

    @Override
    public void delete(Long itemId) {
        Tag tag = entityManager.find(Tag.class, itemId);
        if (tag != null) {
            entityManager.remove(tag);
        }
    }

    @Override
    public List<Tag> findByNewsId(Long newsId) {
        return entityManager.createQuery(FIND_TAGS_BY_NEWS_ID_QUERY)
                .setParameter(NEWS_ID_PARAMETER, newsId)
                .getResultList();
    }

    @Override
    public List<Tag> findByTagsId(List<Long> tagsId) {
        return entityManager.createQuery(FIND_TAG_BY_IDS_QUERY)
                .setParameter(TAGS_ID_PARAMETER, tagsId)
                .getResultList();
    }

    @Override
    public List<Tag> findAll() {
        return entityManager.createQuery(FIND_ALL_TAGS_QUERY).getResultList();
    }

    @Override
    public Tag findById(Long id) {
        return entityManager.find(Tag.class, id);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
