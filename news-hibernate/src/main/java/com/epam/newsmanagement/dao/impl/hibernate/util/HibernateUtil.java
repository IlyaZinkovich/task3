package com.epam.newsmanagement.dao.impl.hibernate.util;


import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class HibernateUtil {

    public static Long insert(Object item, SessionFactory sessionFactory) {
        Session session = sessionFactory.getCurrentSession();
        return (Long) session.save(item);
    }

    public static void update(Object item, SessionFactory sessionFactory) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    public static void deleteObjectOfClass(Long itemId, Class c, SessionFactory sessionFactory) {
        Session session = sessionFactory.getCurrentSession();
        Object o = session.get(c, itemId);
        session.delete(o);
    }

    public static List findAllOfClass(Class c, SessionFactory sessionFactory) {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(c).list();
    }


    public static List findListFromQuery(QueryLambda util, SessionFactory sessionFactory) {
        Session session = sessionFactory.getCurrentSession();
        return util.execute(session).list();
    }

    public static Object findObjectFromQuery(QueryLambda util, SessionFactory sessionFactory) {
        List list = findListFromQuery(util, sessionFactory);
        return list.isEmpty() ? null : list.get(0);
    }

    public static Object findByIdOfClass(Long id, Class c, SessionFactory sessionFactory) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(c, id);
    }

}
