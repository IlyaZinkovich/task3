package com.epam.newsmanagement.dao.impl.hibernate;


import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.impl.hibernate.util.HibernateUtil;
import com.epam.newsmanagement.domain.*;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class NewsDAOImpl implements NewsDAO {

    private static Logger logger = Logger.getLogger(NewsDAOImpl.class);

    private static final String FIND_NEWS_BEGIN = "select distinct n from News n ";
    private static final String FIND_NEWS_ID_LIST_BEGIN = "select distinct n.id from (select n from News n ";
    private static final String FIND_NEWS_COUNT_BEGIN = "select count(distinct n.id) from News n ";
    private static final String FIND_NEWS_BY_AUTHOR = "inner join n.author a ";
    private static final String WHERE_AUTHOR = " a.id=:authorId ";
    private static final String WHERE_TAGS = " t.id in (:tagsId) ";
    private static final String FIND_NEWS_BY_TAGS = "inner join n.tags t ";
    private static final String ORDER_BY_NUMBER_OF_COMMENTS = "order by size(n.comments) desc, n.modificationDate desc, n.title asc) ";
    private static final String WHERE = "where ";
    private static final String AND = "and ";
    private static final String AUTHOR_ID_PARAMETER = "authorId";
    private static final String TAGS_ID_PARAMETER = "tagsId";

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public News findById(Long newsId) {
        return (News) HibernateUtil.findByIdOfClass(newsId, News.class, sessionFactory);
    }

    @Override
    public Long insert(News news) {
        return HibernateUtil.insert(news, sessionFactory);
    }

    @Override
    public void update(News news) {
        HibernateUtil.update(news, sessionFactory);
    }

    @Override
    public void delete(Long newsId) {
        Session session = sessionFactory.getCurrentSession();
        News news = (News) session.get(News.class, newsId);
        if (news != null) {
            news.setComments(null);
            news.setAuthor(null);
            news.setTags(null);
            session.delete(news);
        }
    }

    @Override
    public List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria)  {
        searchCriteria.setBegin(null);
        searchCriteria.setEnd(null);
        List<Long> prevAndNext = new ArrayList<>();
        List<News> newsList = findNews(searchCriteria);
        List<Long> newsIdList = newsList.stream().map(News::getId).collect(Collectors.toList());
        for (int i = 0; i < newsIdList.size(); i++) {
            if (newsIdList.get(i).equals(newsId)) {
                if (i > 0) {
                    prevAndNext.add(newsIdList.get(i - 1));
                } else {
                    prevAndNext.add(0L);
                }
                if (i < newsIdList.size() - 1) {
                    prevAndNext.add(newsIdList.get(i + 1));
                } else {
                    prevAndNext.add(0L);
                }
                return prevAndNext;
            }
        }
        return Arrays.asList(0L, 0L);
//        searchCriteria.setBegin(null);
//        searchCriteria.setEnd(null);
//        List<Long> prevAndNext = new ArrayList<>();
//        List<Long> newsIdList = ((List<News>)HibernateUtil.findListFromQuery((Session session) -> {
//            Query query = session.createQuery(buildFindQuery(searchCriteria, FIND_NEWS_BEGIN));
//            setQueryParameters(searchCriteria, query);
//            return query;
//        }, sessionFactory)).stream().map(News::getId).collect(Collectors.toList());
//        for (int i = 0; i < newsIdList.size(); i++) {
//            if (newsIdList.get(i).equals(newsId)) {
//                if (i > 0) {
//                    prevAndNext.add(newsIdList.get(i - 1));
//                } else {
//                    prevAndNext.add(0L);
//                }
//                if (i < newsIdList.size() - 1) {
//                    prevAndNext.add(newsIdList.get(i + 1));
//                } else {
//                    prevAndNext.add(0L);
//                }
//                return prevAndNext;
//            }
//        }
//        return Arrays.asList(0L, 0L);
    }

    @Override
    public int getNewsCount() {
        return ((Long) sessionFactory.getCurrentSession().createCriteria(News.class)
                .setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public int getNewsCount(SearchCriteria searchCriteria) {
        searchCriteria.setBegin(null);
        searchCriteria.setEnd(null);
        List<Long> newsCountList = HibernateUtil.findListFromQuery((Session session) -> {
            Query query = session.createQuery(buildFindQuery(searchCriteria, FIND_NEWS_COUNT_BEGIN));
            setQueryParameters(searchCriteria, query);
            return query;
        }, sessionFactory);
        return newsCountList.isEmpty() ? 0 : newsCountList.get(0).intValue();
    }

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) {
        return HibernateUtil.findListFromQuery((Session session) -> {
            Query query = session.createQuery(buildFindQuery(searchCriteria, FIND_NEWS_BEGIN));
            setQueryParameters(searchCriteria, query);
            return query;
        }, sessionFactory);
    }

    private void setQueryParameters(SearchCriteria searchCriteria, Query query) {
        if (searchCriteria.getAuthorId() != null) {
            query.setParameter(AUTHOR_ID_PARAMETER, searchCriteria.getAuthorId());
        }
        if (searchCriteria.getTagsId() != null) {
            query.setParameterList(TAGS_ID_PARAMETER, searchCriteria.getTagsId());
        }
        if (searchCriteria.getBegin() != null && searchCriteria.getEnd() != null) {
            query.setFirstResult(searchCriteria.getBegin());
            query.setMaxResults(searchCriteria.getEnd() - searchCriteria.getBegin());
        }
    }

    private String buildFindQuery(SearchCriteria searchCriteria, String findQuery) {
        StringBuilder queryBuilder = new StringBuilder();
        boolean firstWhere = true;
        queryBuilder.append(findQuery);
        if (searchCriteria.getAuthorId() != null) {
            queryBuilder.append(FIND_NEWS_BY_AUTHOR);
        }
        if (searchCriteria.getTagsId() != null) {
            queryBuilder.append(FIND_NEWS_BY_TAGS);
        }
        if (searchCriteria.getAuthorId() != null) {
            queryBuilder.append(firstWhere ? WHERE : AND);
            firstWhere = false;
            queryBuilder.append(WHERE_AUTHOR);
        }
        if (searchCriteria.getTagsId() != null) {
            queryBuilder.append(firstWhere ? WHERE : AND);
            queryBuilder.append(WHERE_TAGS);
        }
        queryBuilder.append(ORDER_BY_NUMBER_OF_COMMENTS);
        return queryBuilder.toString();
    }

    @Override
    public List<News> findAll() {
        return findNews(new SearchCriteria());
    }

}