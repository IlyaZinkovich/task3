package com.epam.newsmanagement.dao.impl.eclipselink;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CommentDAOImpl implements CommentDAO {

    private final static String FIND_ALL_COMMENTS_QUERY = "SELECT c FROM Comment c";
    private final static String FIND_COMMENTS_BY_NEWS_ID_QUERY = "SELECT t FROM News n INNER JOIN n.comments t " +
            "WHERE n.id=:newsId";
    private final static String NEWS_ID_PARAMETER = "newsId";

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<Comment> findByNewsId(Long newsId) {
        return entityManager.createQuery(FIND_COMMENTS_BY_NEWS_ID_QUERY)
                .setParameter(NEWS_ID_PARAMETER, newsId)
                .getResultList();
    }

    @Override
    public Long insert(Comment item) {
        entityManager.persist(item);
        return item.getId();
    }

    @Override
    public void update(Comment item) {
        entityManager.merge(item);
    }

    @Override
    public void delete(Long itemId) {
        Comment comment = entityManager.find(Comment.class, itemId);
        if (comment != null) {
            entityManager.remove(comment);
        }
    }

    @Override
    public List<Comment> findAll() {
        return entityManager.createQuery(FIND_ALL_COMMENTS_QUERY).getResultList();
    }

    @Override
    public Comment findById(Long id) {
        return entityManager.find(Comment.class, id);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
