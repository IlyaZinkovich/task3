package com.epam.newsmanagement.dao.impl.hibernate;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.impl.hibernate.util.HibernateUtil;
import com.epam.newsmanagement.domain.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class AuthorDAOImpl implements AuthorDAO {

    private static final String FIND_AUTHOR_BY_NEWS_ID = "select a from News n " +
            " inner join n.author a where n.id=:newsId";
    private static final String FIND_AUTHOR_NOT_EXPIRED = "select a from Author a where a.expired is null";
    private static final String FIND_AUTHOR_HAVING_NEWS = "select distinct a from News n inner join n.author a ";
    private static final String NEWS_ID_PARAMETER = "newsId";

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Author findByNewsId(Long newsId) {
        return (Author) HibernateUtil.findObjectFromQuery((Session session) ->
                session.createQuery(FIND_AUTHOR_BY_NEWS_ID)
                .setParameter(NEWS_ID_PARAMETER, newsId), sessionFactory);
    }

    @Override
    public List<Author> findNotExpired() {
        return HibernateUtil.findListFromQuery((Session session) ->
                session.createQuery(FIND_AUTHOR_NOT_EXPIRED), sessionFactory);
    }

    @Override
    public List<Author> findHavingNews() {
        return HibernateUtil.findListFromQuery((Session session) ->
                session.createQuery(FIND_AUTHOR_HAVING_NEWS), sessionFactory);
    }

    @Override
    public void update(Long authorId, Date expirationDate) {
        Author author = findById(authorId);
        author.setExpired(expirationDate);
        update(author);
    }

    @Override
    public Long insert(Author item) {
        return HibernateUtil.insert(item, sessionFactory);
    }

    @Override
    public void update(Author item) {
        HibernateUtil.update(item, sessionFactory);
    }

    @Override
    public void delete(Long itemId) {
        Author author = findById(itemId);
        if (author != null) {
            author.setExpired(new Date());
        }
        update(author);
    }

    @Override
    public List<Author> findAll() {
        return HibernateUtil.findAllOfClass(Author.class, sessionFactory);
    }

    @Override
    public Author findById(Long id) {
        return (Author) HibernateUtil.findByIdOfClass(id, Author.class, sessionFactory);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}