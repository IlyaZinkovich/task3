package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.exception.ServiceException;

import java.util.List;

public interface NewsService {

    /**
     * Saves news to the data source and returns the id.
     *
     * @param news
     *        News to add
     *
     * @return generated id
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    long saveNews(News news) throws ServiceException;

    /**
     * Deletes news with the given id from the data source.
     *
     * @param newsId
     *        Id of the news to delete
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    void deleteNews(long newsId) throws ServiceException;

    /**
     * Deletes news with the given id from the data source.
     *
     * @param newsId
     *        The list of news id to delete
     *
     * @throws ServiceException in case of ServiceException
     *
     */
    void deleteNewsList(List<Long> newsId) throws ServiceException;

    /**
     * Returns the news by the given id.
     *
     * @param newsId
     *        Id of the news
     *
     * @return the author by the id of his news
     *
     */
    News findById(long newsId) throws ServiceException;

    /**
     * Returns the count of news.
     *
     * @param searchCriteria
     *         the criteria for news search
     *
     * @return  the count of news
     *
     * @throws  ServiceException
     *          In case of {@code ServiceException}
     */
    List<News> findNews(SearchCriteria searchCriteria) throws ServiceException;

     /**
      * Returns the count of news.
      *
      *
      * @return  the count of news
      *
      * @throws  ServiceException
      *          In case of {@code ServiceException}
      */
    int getNewsCount(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Returns the news id list of two news in the filtered by search criteria list
     * previous and next by one to the news with the given id.
     *
     * @param searchCriteria
     *          the criteria that are used to find news
     *
     * @return  the List of news id,
     *          the first element is 0 if the news with the given id is the first in the whole filtered news list
     *          the second element is 0 if the news with the given id is the last in the whole filtered news list
     *
     * @throws  ServiceException
     *          In case of {@code DAOException}
     */
    List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException;


    /**
     * Returns the list of all the news .
     *
     * @return  the List of all the news
     *
     */
    List<News> findAll();

}
