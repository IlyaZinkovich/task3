package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(isolation= Isolation.DEFAULT,
        propagation= Propagation.REQUIRED)
public class NewsManagementServiceImpl implements NewsManagementService {

    @Autowired
    private NewsService newsService;
    @Autowired
    private CommentService commentService;


    @Override
    public void deleteComment(Long newsId, Long commentId) {
        News news = newsService.findById(newsId);
        Comment comment = commentService.findById(commentId);
        commentService.deleteComment(commentId);
        news.getComments().remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        Long commentId = commentService.addComment(comment);
        comment.setId(commentId);
        News news = newsService.findById(comment.getNews().getId());
        news.getComments().add(comment);
    }

    @Override
    public void deleteNewsList(List<Long> newsList) {
        for (Long newsId : newsList) {
            News news = newsService.findById(newsId);
            news.setComments(null);
        }
        newsService.deleteNewsList(newsList);
    }
}
