package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation= Isolation.DEFAULT,
        propagation= Propagation.REQUIRED)
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);

    @Autowired
    private NewsDAO newsDAO;

    public NewsServiceImpl() {
    }

    public NewsServiceImpl(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    @Override
    public long saveNews(News news) throws ServiceException {
        if (news != null && news.getId() != null) {
            newsDAO.update(news);
            return news.getId();
        } else {
            return newsDAO.insert(news);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void deleteNews(long newsId) throws ServiceException {
        newsDAO.delete(newsId);
    }

    @Override
    public void deleteNewsList(List<Long> newsId) throws ServiceException {
        for (Long id : newsId) {
            newsDAO.delete(id);
        }
    }

    @Override
    public News findById(long newsId) throws ServiceException {
        return newsDAO.findById(newsId);
    }

    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws ServiceException {
        return newsDAO.findNews(searchCriteria);
    }

    @Override
    public int getNewsCount(SearchCriteria searchCriteria) throws ServiceException {
        return newsDAO.getNewsCount(searchCriteria);
    }

    @Override
    public List<Long> findPreviousAndNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
        return newsDAO.findPreviousAndNextNews(newsId, searchCriteria);
    }

    @Override
    public List<News> findAll() {
        return newsDAO.findAll();
    }

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }
}
