package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation= Isolation.DEFAULT,
        propagation= Propagation.REQUIRED)
public class AuthorServiceImpl implements AuthorService {

    private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authorDAO;

    public AuthorServiceImpl() {
    }

    public AuthorServiceImpl(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    @Override
    public long addAuthor(Author author) throws ServiceException {
        return authorDAO.insert(author);
    }

    @Override
    public void editAuthor(Author author) throws ServiceException {
        authorDAO.update(author);
    }

    @Override
    public Author findByNewsId(long newsId) throws ServiceException {
        return authorDAO.findByNewsId(newsId);
    }

    @Override
    public Author findById(long authorId) throws ServiceException {
        return authorDAO.findById(authorId);
    }

    @Override
    public List<Author> findAll() throws ServiceException {
        return authorDAO.findAll();
    }

    @Override
    public List<Author> findNotExpired() throws ServiceException {
        return authorDAO.findNotExpired();
    }

    @Override
    public List<Author> findHavingNews() throws ServiceException {
        return authorDAO.findHavingNews();
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }
}
