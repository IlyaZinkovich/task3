package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation= Isolation.DEFAULT,
        propagation= Propagation.REQUIRED)
public class TagServiceImpl implements TagService {

    private static final Logger logger = Logger.getLogger(TagServiceImpl.class);

    @Autowired
    private TagDAO tagDAO;

    public TagServiceImpl() {
    }

    public TagServiceImpl(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    @Override
    public long addTag(Tag tag) throws ServiceException {
        return tagDAO.insert(tag);
    }

    @Override
    public void editTag(Tag tag) throws ServiceException {
        tagDAO.update(tag);
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void deleteTag(long tagId) throws ServiceException {
        tagDAO.delete(tagId);
    }

    @Override
    public List<Tag> findByNewsId(long newsId) throws ServiceException {
        return tagDAO.findByNewsId(newsId);
    }

    @Override
    public List<Tag> findAll() throws ServiceException {
        return tagDAO.findAll();
    }

    @Override
    public List<Tag> findByTagsId(List<Long> tagsId) {
        return tagDAO.findByTagsId(tagsId);
    }

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }
}
