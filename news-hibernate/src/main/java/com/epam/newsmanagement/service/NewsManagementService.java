package com.epam.newsmanagement.service;


import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;

import java.util.List;

public interface NewsManagementService {

    void deleteComment(Long newsId, Long commentId);

    void addComment(Comment comment);

    void deleteNewsList(List<Long> newsList);

}
