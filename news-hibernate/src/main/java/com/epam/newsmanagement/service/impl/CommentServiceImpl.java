package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation= Isolation.DEFAULT,
        propagation= Propagation.REQUIRED)
public class CommentServiceImpl implements CommentService {

    private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);

    @Autowired
    private CommentDAO commentDAO;

    public CommentServiceImpl() {
    }

    public CommentServiceImpl(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    @Override
    public long addComment(Comment comment) throws ServiceException {
        return commentDAO.insert(comment);
    }

    @Override
    public void editComment(Comment comment) throws ServiceException {
        commentDAO.update(comment);
    }

    @Override
    public List<Comment> findByNewsId(long newsId) throws ServiceException {
        return commentDAO.findByNewsId(newsId);
    }

    @Override
    public Comment findById(long commentId) throws ServiceException {
        return commentDAO.findById(commentId);
    }

    @Override
    public void deleteComment(long commentId) throws ServiceException {
        commentDAO.delete(commentId);
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

}
