package com.epam.newsmanagement.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tag")
public class Tag implements Serializable {

    private static final long serialVersionUID = 5761023493727149076L;

    @Id
    @SequenceGenerator(name="TAG_AI", sequenceName="TAG_AI", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAG_AI")
    @Column(name = "tag_id")
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "tag_name")
    private String name;

    @ManyToMany(mappedBy="tags")
    private Set<News> news;

    public Tag() {
    }

    public Tag(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o != null && o.getClass() != this.getClass()) return false;

        Tag tag = (Tag) o;

        if (!Objects.equals(id, tag.id)) return false;
        if (!name.equals(tag.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
