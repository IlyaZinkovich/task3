package com.epam.newsmanagement.domain;

import java.util.List;

public class SearchCriteria {
    private Long authorId;
    private List<Long> tagsId;
    private Integer begin;
    private Integer end;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Integer getBegin() {
        return begin;
    }

    public void setBegin(Integer begin) {
        this.begin = begin;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public int calculateParameterNumber() {
        int parameterNumber = 0;
        parameterNumber += authorId == null ? 0 : 1;
        parameterNumber += tagsId == null ? 0 : tagsId.size();
        parameterNumber += begin == null ? 0 : 2;
        parameterNumber += end == null ? 0 : 2;
        return parameterNumber;
    }

}
