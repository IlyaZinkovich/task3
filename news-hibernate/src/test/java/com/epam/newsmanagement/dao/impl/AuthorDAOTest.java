package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-test.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dbunitxml/news-data.xml")
@Transactional
public class AuthorDAOTest {

	//Tests are crushing because of em.getTransaction()

	private static Logger logger = Logger.getLogger(AuthorDAOTest.class);

	@Autowired
	private AuthorDAO authorDAO;

	private Author testAuthor;

	@Test
	public void findAuthorByNewsId() throws Exception {
		authorDAO.findByNewsId(1L);
	}

	@Before
	public void setUp() {
		testAuthor = new Author(null, "Bill", null);
	}


	@Test
	public void findAll() throws Exception {
		List<Author> authorList = authorDAO.findAll();
		assertNotNull(authorList.size());
	}

	@Test
	public void findById() throws Exception {
		Long id = 1L;
		Author author = authorDAO.findById(id);
		assertEquals(author.getId(), id);
	}

	@Test
	public void findNotExpired() throws Exception {
		List<Author> authorList = authorDAO.findNotExpired();
		assertNotNull(authorList.size());
	}

	@Test
	public void insertAuthorSucceed() throws Exception {
		long generatedId = authorDAO.insert(testAuthor);
		assertNotNull(generatedId);
		testAuthor.setId(generatedId);
		assertEquals(testAuthor, authorDAO.findById(generatedId));
	}

	@Test
	public void updateAuthorSucceed() throws Exception {
		Author authorToUpdate = authorDAO.findById(1L);
		authorToUpdate.setExpired(new Date());
		authorToUpdate.setName(testAuthor.getName());
		authorDAO.update(authorToUpdate);
		Author updatedAuthor = authorDAO.findById(authorToUpdate.getId());
		assertEquals(authorToUpdate, updatedAuthor);
	}

	@Test
	public void findByNewsIdSucceed() throws Exception {
		Author foundAuthor = authorDAO.findByNewsId(5L);
		assertNotNull(foundAuthor);
	}

	@Test
	public void findHavingNewsSucceed() throws Exception {
		List<Author> authorList = authorDAO.findHavingNews();
		assertEquals(authorList.size(), 6);
	}

	@Test
	public void deleteAuthorSucceed() throws Exception {
		authorDAO.delete(5L);
		assertNotNull(authorDAO.findById(5L).getExpired());
	}


}
