package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-test.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dbunitxml/news-data.xml")
@Transactional
public class CommentDAOTest {

    private static Logger logger = Logger.getLogger(CommentDAOTest.class);

    @Autowired
    private CommentDAO commentDAO;

    private Comment testComment;
    private List<Comment> testCommentsList;

    @Before
    public void setUp() {
        testCommentsList = new LinkedList<>();
        testCommentsList.add(new Comment(null, "1111", new Date()));
        testCommentsList.add(new Comment(null, "2222", new Date()));
    }

//    @Test
//    public void findAllSucceed() throws Exception {
//        List<Comment> foundComments = commentDAO.findAll();
//        assertEquals(22, foundComments.size());
//    }

    @Test
    public void insertCommentSucceed() throws Exception {
//        Comment c = new Comment(null, "1111", new Date());
//        long generatedId = commentDAO.insert(c);
//        assertNotNull(generatedId);
//        assertEquals(c, commentDAO.findById(generatedId));
    }

    @Test
    public void updateCommentSucceed() throws Exception {
        Comment commentToUpdate = commentDAO.findAll().get(0);
        commentToUpdate.setCreationDate(new Date());
        commentToUpdate.setCommentText("test");
//        commentToUpdate.setNewsId(1L);
        commentDAO.update(commentToUpdate);
        Comment updatedComment = commentDAO.findById(commentToUpdate.getId());
        assertEquals(commentToUpdate, updatedComment);
    }

    @Test
    public void deleteCommentSucceed() throws Exception {
        Comment commentToDelete = commentDAO.findAll().get(0);
        commentDAO.delete(commentToDelete.getId());
        assertNull(commentDAO.findById(commentToDelete.getId()));
    }

//    @Test
//    public void findByNewsIdSucceed() throws Exception {
//        List<Comment> foundComments = commentDAO.findByNewsId(1L);
//        assertEquals(5, foundComments.size());
//    }

    @Test
    public void findByNewsIdReturnsNullIfNewsDoesNotHaveComments() throws Exception {
        List<Comment> foundComments = commentDAO.findByNewsId(8L);
        assertEquals(Collections.EMPTY_LIST, foundComments);
    }


}

