package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-test.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dbunitxml/news-data.xml")
@Transactional
public class TagDAOTest {

    private static Logger logger = Logger.getLogger(TagDAOTest.class);

    @Autowired
    private TagDAO tagDAO;

    private List<Tag> testTagsList;

    @Before
    public void setUp() {
        testTagsList = new LinkedList<>();
    }

    @Test
    public void findAllSucceed() throws Exception {
        List<Tag> foundTags = tagDAO.findAll();
        assertEquals(12, foundTags.size());
    }

    @Test
    public void insertTagSucceed() throws Exception {
        long generatedId = tagDAO.insert(new Tag(null, "test"));
        assertNotNull(generatedId);
    }

    @Test
    public void updateTagSucceed() throws Exception {
        Tag tagToUpdate = tagDAO.findById(2L);
        tagToUpdate.setName("test");
        tagDAO.update(tagToUpdate);
        Tag updatedTag = tagDAO.findById(tagToUpdate.getId());
        assertEquals(updatedTag, tagToUpdate);
    }

    @Test
    public void deleteTagSucceed() throws Exception {
        tagDAO.delete(1L);
        assertNull(tagDAO.findById(1L));
    }

    @Test
    public void findByNewsIdSucceed() throws Exception {
        List<Tag> foundTags = tagDAO.findByNewsId(1L);
        assertEquals(3, foundTags.size());
    }



}

