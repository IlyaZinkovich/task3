package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-test.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dbunitxml/news-data.xml")
@Transactional
public class NewsDAOTest {

    private static Logger logger = Logger.getLogger(NewsDAOTest.class);

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private NewsDAO newsDAO;


    @Test
    public void findByIdSucceed() throws Exception {
        News news = newsDAO.findById(2L);
        assertNotNull(news);
        assertEquals(Long.valueOf(2L), news.getId());
    }

    @Test
    public void insertNewsSucceed() throws Exception {
        News news = new News(null, "13", "13", "13", new Date(), new Date());
        Long newsId = newsDAO.insert(news);
        assertNotNull(newsId);
    }

    @Test
    public void updateNewsSucceed() throws Exception {
        News news = newsDAO.findById(2L);
        news.setFullText("fullll");
        newsDAO.update(news);
        News updatedNews = newsDAO.findById(news.getId());
        assertEquals(news, updatedNews);
    }


    @Test
    public void findAllSucceed() throws Exception {
        List<News> foundNews = newsDAO.findNews(new SearchCriteria());
        assertNotNull(foundNews);
        assertEquals(13, foundNews.size());
    }

    @Test
    public void findByAuthorSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(Arrays.asList(3L, 4L, 5L), foundNewsIdList);
    }

    @Test
    public void findByAuthorReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(13L);
        List foundNewsList = newsDAO.findNews(sc);
        assertEquals(Collections.EMPTY_LIST, foundNewsList);
    }

    @Test
    public void findByTagsSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setTagsId(Arrays.asList(3L, 4L, 5L));
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(Arrays.asList(1L, 2L, 3L, 4L, 6L, 7L, 9L, 10L), foundNewsIdList);
    }

    @Test
    public void findByTagsReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(13L);
        List foundNewsList = newsDAO.findNews(sc);
        assertEquals(Collections.EMPTY_LIST, foundNewsList);
    }

    @Test
    public void findByAuthorAndTagsSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 4L, 6L));
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(Arrays.asList(3L, 4L), foundNewsIdList);
    }

    @Test
    public void findByAuthorAndTagsSucceedReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 2L));
        List foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        assertEquals(Collections.EMPTY_LIST, foundNewsList);
    }

    @Test
    public void findByPageOfPageSizeSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setBegin(2);
        sc.setEnd(6);
        List<News> newsList = newsDAO.findNews(sc);
        assertNotNull(newsList);
        assertEquals(newsList.size(), 4);
    }

    @Test
    public void findByPageOfPageSizeReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setBegin(50);
        sc.setEnd(55);
        List newsList = newsDAO.findNews(sc);
        assertNotNull(newsList);
        assertEquals(newsList, Collections.EMPTY_LIST);
    }

    @Test
    public void findByAuthorAndTagsForPageOfSizeSucceed() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 4L, 6L));
        sc.setBegin(0);
        sc.setEnd(4);
        List<News> foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        List<Long> foundNewsIdList = foundNewsList.stream().map(News::getId)
                .sorted().collect(Collectors.toList());
        assertEquals(Arrays.asList(3L, 4L), foundNewsIdList);
    }


    @Test
    public void findByAuthorAndTagsForPageOfSizeReturnsEmptyListIfNewsNotFound() throws Exception {
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(2L);
        sc.setTagsId(Arrays.asList(1L, 4L, 6L));
        sc.setBegin(8);
        sc.setEnd(12);
        List foundNewsList = newsDAO.findNews(sc);
        assertNotNull(foundNewsList);
        assertEquals(Collections.EMPTY_LIST, foundNewsList);
    }

    @Test
    public void getNewsCountSucceed() throws Exception {
        int newsCount = newsDAO.getNewsCount();
        assertNotNull(newsCount);
        assertEquals(13, newsCount);
    }

    @Test
    public void findPreviousAndNextNewsSucceed() throws Exception {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        List<Long> previousAndNextNews = newsDAO.findPreviousAndNextNews(1L, searchCriteria)
//                .stream().sorted().collect(Collectors.toList());
//        assertNotNull(previousAndNextNews);
//        assertEquals(Arrays.asList(2L, 4L), previousAndNextNews);
    }

    @Test
    public void findPreviousAndNextNewsReturnZeroAsPreviousIfNewsIsFirst() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> allNewsList = newsDAO.findNews(searchCriteria);
        List<Long> previousAndNextNews = newsDAO.findPreviousAndNextNews(allNewsList.get(0).getId(), searchCriteria)
                .stream().collect(Collectors.toList());
        assertNotNull(previousAndNextNews);
        assertEquals(Arrays.asList(0L, allNewsList.get(1).getId()), previousAndNextNews);
    }

    @Test
    public void findPreviousAndNextNewsReturnZeroAsPreviousIfNewsIsLast() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> allNewsList = newsDAO.findNews(searchCriteria);
        List<Long> previousAndNextNews = newsDAO.findPreviousAndNextNews(allNewsList.get(allNewsList.size() - 1).getId(),
                searchCriteria).stream().collect(Collectors.toList());
        assertNotNull(previousAndNextNews);
        assertEquals(Arrays.asList(allNewsList.get(allNewsList.size() - 2).getId(), 0L), previousAndNextNews);
    }

    @Test
    public void deleteSucceed() throws Exception {
        newsDAO.delete(1L);
        assertNull(newsDAO.findById(1L));
    }

    @Test
    public void deleteWithConstraintsSucceed() throws Exception {
        News news = newsDAO.findById(2L);
        news.getComments().size();
        news.getAuthor().getId();
        news.getTags().size();
        newsDAO.delete(2L);
        assertNull(newsDAO.findById(2L));
    }

}
