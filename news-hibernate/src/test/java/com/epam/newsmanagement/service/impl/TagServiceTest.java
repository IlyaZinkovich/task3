package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {

    @Mock
    private TagDAO tagDAO;

    @InjectMocks
    private TagServiceImpl tagService;

    private Tag testTag;
    private List<Tag> testTags;
    private List<Long> testGeneratedIdList;
    private long testNewsId;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        testTag = new Tag(1l, "tag");
        testTags = new LinkedList<>();
        testTags.add(testTag);
        testTags.add(new Tag(2l, "second"));
        testTags.add(new Tag(3l, "third"));
        testGeneratedIdList = LongStream.rangeClosed(1, testTags.size()).mapToObj(l -> l)
                .collect(Collectors.toList());
        testNewsId = 1;
    }

    @Test
    public void addTagSucceed() throws Exception {
        when(tagDAO.insert(testTag)).thenReturn(1l);
        long generatedId = tagService.addTag(testTag);
        assertNotNull(generatedId);
        verify(tagDAO).insert(testTag);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test
    public void editTagSucceed() throws Exception {
        tagService.editTag(testTag);
        verify(tagDAO).update(testTag);
        verifyNoMoreInteractions(tagDAO);
    }

    @Test
    public void deleteTagSucceed() throws Exception {
        tagService.deleteTag(testTag.getId());
        verify(tagDAO).delete(testTag.getId());
        verifyNoMoreInteractions(tagDAO);
    }


    @Test
    public void findByNewsIdSucceed() throws Exception {
        when(tagDAO.findByNewsId(testNewsId)).thenReturn(testTags);
        List<Tag> foundTags = tagService.findByNewsId(testNewsId);
        assertEquals(testTags, foundTags);
        verify(tagDAO).findByNewsId(testNewsId);
        verifyNoMoreInteractions(tagDAO);
    }
}
