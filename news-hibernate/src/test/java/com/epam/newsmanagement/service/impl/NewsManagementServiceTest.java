package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsManagementServiceTest {

    @Autowired
    private NewsService newsService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private CommentService commentService;

    @Test
    public void addCommentSucceed() throws Exception {
//        String commentText = "tt";
//        Long newsId = 2L;
//        Comment comment = new Comment(null, commentText, new Date());
//        comment.setNewsId(newsId);
//        Long cId = commentService.addComment(comment);
//        comment.setId(cId);
//        News news = newsService.findById(newsId);
//        news.getComments().add(comment);
////        news.getComments().size();
//        news = newsService.findById(newsId);
//        System.out.println(news.getComments().size());
//        assertTrue(news.getComments().contains(comment));
    }

    @Test
    public void deleteCommentSucceed() throws Exception {
//        Long newsId = 1L;
//        News news = newsService.findById(newsId);
//        Long commentId = news.getComments().get(0).getId();
//        Comment comment = commentService.findById(commentId);
//        news.getComments().remove(comment);
//        commentService.deleteComment(commentId);
//        news = newsService.findById(newsId);
//        assertFalse(news.getComments().contains(comment));
    }

    @Test
    public void addAuthor() throws Exception {
        Long authorId = addA();
        authorService.findById(authorId);
    }

    private Long addA() {
        Author author = new Author(null, "author", null);
        return authorService.addAuthor(author);
    }

}
