package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {

    @Mock
    private AuthorDAO authorDAO;

    @InjectMocks
    private AuthorServiceImpl authorService;

    private Author testAuthor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        testAuthor = new Author(1l, "John", null);
    }

    @Test
    public void addAuthorSucceed() throws Exception {
        when(authorDAO.insert(testAuthor)).thenReturn(1l);
        long generatedId = authorService.addAuthor(testAuthor);
        assertNotNull(generatedId);
        verify(authorDAO).insert(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void editAuthorSucceed() throws Exception {
        authorService.editAuthor(testAuthor);
        verify(authorDAO).update(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }


    @Test
    public void findByNewsIdSucceed() throws Exception {
        when(authorDAO.findByNewsId(1L)).thenReturn(testAuthor);
        authorService.findByNewsId(1);
        verify(authorDAO).findByNewsId(1L);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void findByIdSucceed() throws Exception {
        when(authorDAO.findById(testAuthor.getId())).thenReturn(testAuthor);
        authorService.findById(testAuthor.getId());
        verify(authorDAO).findById(testAuthor.getId());
        verifyNoMoreInteractions(authorDAO);
    }
}
