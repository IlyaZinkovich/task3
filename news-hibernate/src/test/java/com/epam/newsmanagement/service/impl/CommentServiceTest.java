package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {

    @Mock
    private CommentDAO commentDAO;

    @InjectMocks
    private CommentServiceImpl commentService;

    private Comment testComment;
    private List<Comment> testComments;
    private List<Long> testGeneratedIdList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testComment = new Comment(1l, "comment", new Date());
        testComments = new LinkedList<>();
        testComments.add(testComment);
        testComments.add(new Comment(2l, "first", new Date()));
        testGeneratedIdList = LongStream.rangeClosed(1, testComments.size()).mapToObj(l -> l)
                .collect(Collectors.toList());
    }

    @Test
    public void addCommentSucceed() throws Exception {
        when(commentDAO.insert(testComment)).thenReturn(1l);
        long generatedId = commentService.addComment(testComment);
        assertNotNull(generatedId);
        verify(commentDAO).insert(testComment);
        verifyNoMoreInteractions(commentDAO);
    }


    @Test
    public void editCommentSucceed() throws Exception {
        commentService.editComment(testComment);
        verify(commentDAO).update(testComment);
        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void deleteCommentSucceed() throws Exception {
        commentService.deleteComment(testComment.getId());
        verify(commentDAO).delete(testComment.getId());
        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void findByNewsIdSucceed() throws Exception {
//        when(commentDAO.findByNewsId(testComment.getNewsId())).thenReturn(testComments);
//        commentService.findByNewsId(testComment.getNewsId());
//        verify(commentDAO).findByNewsId(testComment.getNewsId());
//        verifyNoMoreInteractions(commentDAO);
    }

    @Test
    public void findByIdSucceed() throws Exception {
        commentService.findById(testComment.getId());
        verify(commentDAO).findById(testComment.getId());
        verifyNoMoreInteractions(commentDAO);
    }

}
