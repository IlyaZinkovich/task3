package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.forms.*;
import com.epam.newsmanagement.service.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({"filter"})
public class NewsController {

    public final static Integer DEFAULT_PAGE_SIZE = 4;
    private static Logger logger = Logger.getLogger(NewsController.class);

    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private NewsManagementService newsManagementService;

    @ModelAttribute("filter")
    public Filter initFilterForm() {
        return new Filter();
    }


    @ModelAttribute("addUpdateNewsForm")
    public AddUpdateNewsForm initAddUpdateNewsForm() {
        return new AddUpdateNewsForm();
    }

    @ModelAttribute("newsIdList")
    public List<Long> initNewsIdToDeleteForm() {
        return new LinkedList<>();
    }


    @RequestMapping(value = "/add-update-news", method = RequestMethod.POST)
    public String addNews(Model model, @ModelAttribute AddUpdateNewsForm form, BindingResult bindingResult) throws Exception {
        try {
            Long newsId = form.getNewsId();
            News news;
            if (newsId != null) {
                news = newsService.findById(newsId);
            } else {
                news = new News();
            }
            news.setShortText(form.getBrief());
            news.setFullText(form.getContent());
            news.setTitle(form.getTitle());
            news.setCreationDate(form.getCreationDate());
            news.setModificationDate(form.getModificationDate());
            news.getComments();
            Author author = authorService.findById(form.getAuthorId());
            List<Tag> tags = tagService.findByTagsId(form.getTagsId());
            news.setAuthor(author);
            news.setTags(tags);
            newsService.saveNews(news);
            List<News> newsList = newsService.findNews(new SearchCriteria());
            newsList.add(news);
            model.addAttribute("newsList", newsList);
            return "redirect:/view-news/" + news.getId();
        } catch (HibernateException e) {
            model.addAttribute("concurrent", true);
            return "redirect:/add-update-news";
        }
    }

    @RequestMapping(value = "/add-update-news", method = RequestMethod.GET)
    public String addUpdateNews(Model model) {
        List<Author> authorList = authorService.findNotExpired();
        List<Tag> tagList = tagService.findAll();
        AddUpdateNewsForm addUpdateNewsForm = new AddUpdateNewsForm();
        addUpdateNewsForm.setCreationDate(new Date());
        model.addAttribute("news", addUpdateNewsForm);
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("current", "add-update-news");
        return "add-update-news";
    }

    @RequestMapping(value = "/add-update-news/{newsId}", method = RequestMethod.GET)
    public String updateNews(Model model, @PathVariable Long newsId) {
        News news = newsService.findById(newsId);
        if (news == null) {
            return "redirect:/add-update-news";
        }
        fillAddUpdateNewsForm(model, news);
        model.addAttribute("current", "add-update-news");
        return "add-update-news";
    }

    private void fillAddUpdateNewsForm(Model model, News news) {
        AddUpdateNewsForm form = new AddUpdateNewsForm();
        form.setNewsId(news.getId());
        form.setTitle(news.getTitle());
        form.setBrief(news.getShortText());
        form.setContent(news.getFullText());
        form.setCreationDate(news.getCreationDate());
        form.setAuthorId(news.getAuthor().getId());
        form.setTagsId(news.getTags().stream().map(Tag::getId).collect(Collectors.toList()));
        List<Author> authorList = authorService.findNotExpired();
        authorList.add(news.getAuthor());
        List<Tag> tagList = tagService.findAll();
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("news", form);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model, @ModelAttribute Filter filter) {
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/news-list", method = RequestMethod.GET)
    public String newsList(Model model, @ModelAttribute Filter filter) {
        return "redirect:/news-list/1";
    }

    @RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
    public String newsListPaged(Model model, @PathVariable Integer page, @ModelAttribute Filter filter) {
        SearchCriteria searchCriteria = filterToSearchCriteria(page, filter);
        List<News> newsList = newsService.findNews(searchCriteria);
        List<Author> authorList = authorService.findHavingNews();
        List<Tag> tagList = tagService.findAll();
        int newsCount = newsService.getNewsCount(searchCriteria);
        model.addAttribute("newsCount", newsCount);
        Integer pageCount = (int) Math.ceil((double) newsCount / DEFAULT_PAGE_SIZE);
        fillModelAttributesForNewsList(model, filter, newsList, authorList, tagList, pageCount);
        return "news-list";
    }

    private void fillModelAttributesForNewsList(Model model, @ModelAttribute Filter filter, List<News> newsList, List<Author> authorList, List<Tag> tagList, Integer pageCount) {
        model.addAttribute("newsList", newsList);
        model.addAttribute("tagList", tagList);
        model.addAttribute("authorList", authorList);
        model.addAttribute("pageCount", pageCount);
        model.addAttribute("filter", filter);
        model.addAttribute("current", "news-list");
    }

    private SearchCriteria filterToSearchCriteria(Integer page, Filter filter) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(filter.getAuthorId());
        searchCriteria.setTagsId(filter.getTagsId());
        if (page != null) {
            searchCriteria.setBegin((page - 1) * DEFAULT_PAGE_SIZE);
            searchCriteria.setEnd(page * DEFAULT_PAGE_SIZE);
        }
        return searchCriteria;
    }

    @RequestMapping(value = "/news-list/delete", method = RequestMethod.POST)
    public String deleteNewsList(@RequestParam("newsIdToDelete") List<Long> newsIdToDelete) {
        newsManagementService.deleteNewsList(newsIdToDelete);
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/view-news/{id}", method = RequestMethod.GET)
    public String viewNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        News news = newsService.findById(id);
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        addPreviousAndNextNewsIdToTheModel(model, previousAndNextNews);
        model.addAttribute("news", news);
        return "view-news";
    }

    private void addPreviousAndNextNewsIdToTheModel(Model model, List<Long> previousAndNextNews) {
        if (previousAndNextNews.get(0) != null) {
            model.addAttribute("previous", previousAndNextNews.get(0));
        }
        if (previousAndNextNews.get(1) != null) {
            model.addAttribute("next", previousAndNextNews.get(1));
        }
    }

    @RequestMapping(value = "/view-news/{news-id}/delete-comment", method = RequestMethod.POST)
    public String deleteComment(@RequestParam(value = "commentId") Long commentId,
                                @RequestParam(value = "newsId") Long newsId) {
        newsManagementService.deleteComment(newsId, commentId);
        return "redirect:/view-news/" + newsId;
    }

    @RequestMapping(value = "/view-news/{newsId}/add-comment", method = RequestMethod.POST)
    public String addComment(@PathVariable Long newsId, Model model,
                             @RequestParam(value = "commentText") String commentText) throws Exception {
        if (commentText == null) {
            return "redirect:/view-news/" + newsId;
        }
        Comment comment = new Comment(null, commentText, new Date());
        News news = newsService.findById(newsId);
        comment.setNews(news);
        newsManagementService.addComment(comment);
        model.addAttribute("news", news);
        return "redirect:/view-news/" + newsId;
    }

    @RequestMapping(value = "/view-news/{id}/previous")
    public String previousNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        Long previousNewsId = previousAndNextNews.get(0);
        News news = newsService.findById(previousNewsId);
        model.addAttribute("news", news);
        return "redirect:/view-news/" + previousNewsId;
    }

    @RequestMapping(value = "/view-news/{id}/next")
    public String nextNews(@PathVariable Long id, Model model, @ModelAttribute Filter filter) {
        List<Long> previousAndNextNews = newsService.findPreviousAndNextNews(id, filterToSearchCriteria(null, filter));
        Long previousNewsId = previousAndNextNews.get(1);
        News news = newsService.findById(previousNewsId);
        model.addAttribute("news", news);
        return "redirect:/view-news/" + previousNewsId;
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.POST)
    public String filterNews(Model model, @ModelAttribute Filter filter, BindingResult result) {
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/news-list/filter", method = RequestMethod.GET)
    public String showFilteredNews() {
        return "redirect:/news-list";
    }

    @RequestMapping(value = "/news-list/filter/reset", method = RequestMethod.GET)
    public String showNews(Model model) {
        model.addAttribute("filter", new Filter());
        return "redirect:/news-list";
    }
}
