<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../pages/taglib.jsp"%>
<!DOCTYPE HTML SYSTEM>
<html>
    <head>
        <link rel="stylesheet" href="<c:url value='/resources/css/datepicker.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-multiselect.css'/>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
        <script src="<c:url value='/resources/js/bootstrap-multiselect.js'/>"></script>
        <title><tiles:getAsString name="title"/></title>
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    </head>

    <body>
        <div class="container">
            <tiles:insertAttribute name="header"/>
            <div class="row">
                <div class="col-md-2 sidebar">
                    <tiles:insertAttribute name="menu"/>
                </div>
                <div class="col-md-10 main">
                    <tiles:insertAttribute name="body"/>
                </div>
            </div>
        </div>
        <tiles:insertAttribute name="footer"/>
    </body>
</html>