<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>

<link rel="stylesheet" type="text/css" href="<c:url value='/resources/selectpicker/dist/css/bootstrap-select.css'/>">
<script type="text/javascript" src="<c:url value='/resources/selectpicker/dist/js/bootstrap-select.js'/>"></script>


<script type="text/javascript">
        $(document).ready(function() {
            $(".selectpicker").selectpicker();
        });
</script>
<div class="form-group">
    <form:select multiple="multiple" path="tagsId" cssClass="selectpicker" >
        <form:options items="${tagList}" itemLabel="name" itemValue="id" />
    </form:select>
</div>