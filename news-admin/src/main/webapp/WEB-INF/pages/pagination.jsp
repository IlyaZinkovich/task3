<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<div class="row clearfix">
<div class="col-md-4 column">
</div>
<div class="col-md-8 column">
    <ul class="pagination">
        <c:forEach var="i" begin="1" end="${pageCount}">
            <li>
                <a href='<spring:url value="/news-list/${i}.html" />' /><c:out value="${i}"/></a>
            </li>
        </c:forEach>
    </ul>
</div>
</div>