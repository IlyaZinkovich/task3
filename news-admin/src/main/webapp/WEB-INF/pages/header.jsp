<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<div class="row clearfix">
    <div class="col-md-12 column">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="<spring:url value="/news-list"/>"><spring:message code='header.logo' /></a>
            </div>
            <a href="<spring:url value="/logout"/>" class="btn pull-right"><spring:message code='header.logout' /></a></li>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="?locale=ru">RU</a>
                    </li>
                    <li>
                       <a href="?locale=en">EN</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>