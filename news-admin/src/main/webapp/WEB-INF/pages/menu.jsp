<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<ul class="nav nav-pills nav-stacked">
    <li class="${current == 'news-list' ? 'active' : ''}"><a href="<spring:url value="/news-list"/>"><spring:message code='menu.news-list' /></a></li>
    <li class="${current == 'add-update-news' ? 'active' : ''}"><a href="<spring:url value="/add-update-news"/>"><spring:message code='menu.add-news' /></a></li>
    <li class="${current == 'add-update-authors' ? 'active' : ''}"><a href="<spring:url value="/add-update-authors"/>"><spring:message code='menu.add-update-authors' /></a></li>
    <li class="${current == 'add-update-tags' ? 'active' : ''}"><a href="<spring:url value="/add-update-tags"/>"><spring:message code='menu.add-update-tags' /></a></li>
</ul>
