<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<head>
  <title>Log in</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<style>
  .form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox {
    margin-bottom: 10px;
  }
  .form-signin .checkbox {
    font-weight: normal;
  }
  .form-signin .form-control {
    position: relative;
    height: auto;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 10px;
    font-size: 16px;
  }
  .form-signin .form-control:focus {
    z-index: 2;
  }
  .form-signin input[type="name"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
</style>
<div class="container">
  <div class="row">
    <form class="form-signin" role="form" action='<spring:url value="/j_spring_security_check" />' method="POST">
      <h2 class="form-signin-heading"><spring:message code='login.login' /></h2>
      <label for="name" class="sr-only"><spring:message code='login.name' /></label>
      <input type="text" name="j_username" id="name" class="form-control" placeholder="<spring:message code='login.name.placeholder' />" required autofocus>
      <br>
      <label for="password" class="sr-only"><spring:message code='login.password' /></label>
      <input type="password" name="j_password" id="password" class="form-control" placeholder="<spring:message code='login.password.placeholder' />" required>
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code='login.log-in' /></button>
    </form>
  </div>
</div>